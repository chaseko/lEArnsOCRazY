#include <iostream>

int main() {

    auto a = 1, b = 2, c = 3;

    /* basic lambda func */
    auto hello = []() { std::cout << "hello lambda!" << std::endl; };
    hello();

    /* captured by value */
    auto add = [a, b]() { return a + b; };
    std::cout << add() << std::endl;

    /* captured by reference, pass two integer be parameter, mutable to change reference value */
    auto pass_val = [&c](int x, int y) mutable {
        c = 100;
        auto ret = x + y + c;
        std::cout << "ret is: " << ret << std::endl;
    };
    pass_val(1, 10);
    std::cout << "after changing... c would be: " << c << std::endl;

    

    return 1;
}
