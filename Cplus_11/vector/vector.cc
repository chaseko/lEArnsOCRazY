#include <iostream>
#include <vector>

int main() {

    std::vector<int> ivec1;
    std::cout << "ivec1 is " << (ivec1.empty() ? "empty" : "not empty") << std::endl;
    std::cout << std::endl;

    ivec1.push_back(1);
    std::cout << "the ivec1[0] is: " << ivec1.at(0) << std::endl;
    std::cout << std::endl;

    std::vector<int> ivec2(ivec1);
    std::cout << "the ivec2 copy from ivec1 have the same element ivec2[0]: " << ivec2[0] << std::endl;
    std::cout << std::endl;

    std::vector<int> ivec3(3, 1);
    std::cout << "initialize the ivec3 with size 3 and each element value is `1` like following:" << std::endl;
    for (auto &&i: ivec3)
        std::cout << i << " ";
    std::cout << std::endl << std::endl;

    std::vector<int> ivec4(3);
    std::cout << "the empty ivec4 length is: " << ivec4.size() << std::endl;
    std::cout << std::endl;

    return 1;
}

