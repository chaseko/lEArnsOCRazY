/*
 * features you can use are included on c++11
 *
 * Given a sorted array and a target value, return the index if the target is found.
 * If not, return the index where it would be if it were inserted in order.
 *
 * You may assume no duplicates in the array.
 * Here are few examples.
 * [1,3,5,6], 5 ¡÷ 2
 * [1,3,5,6], 2 ¡÷ 1
 * [1,3,5,6], 7 ¡÷ 4
 * [1,3,5,6], 0 ¡÷ 0
 *
 * ref: https://leetcode.com/problems/search-insert-position/
 *
 */
#include <iostream>
// include what you want....
#include <vector>
#include <map>
using namespace std;
int main()
{
    int i;
    vector<int> vec1 {1, 3, 5, 6};
    auto target=0;
    // first is target, second is store position
    map<int, int> ret;
    // result should be {5, 2}
    cout << "insert a value" <<endl;
    cin>> target;
    for(i=0; i<=vec1.size();i++){
            if(vec1[i]>=target ){
                  ret.insert(pair<int, int>(target, i));
            }
    }
    for (auto it = ret.cbegin(); it != ret.cend(); ++it){
            cout << "target: "  << (*it).first <<  ", store position: "<< (*it).second <<endl ;
            }
    for (auto& x: ret) {
            cout << x.first << ": " << x.second << '\n';
            }
    for (std::map<int,int>::iterator it=ret.begin(); it!=ret.end(); ++it)
    cout << it->first << " => " << it->second << '\n';
    return 1;
}
