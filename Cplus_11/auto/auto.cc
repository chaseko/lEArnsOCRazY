#include <vector>
#include <iostream>
#include <typeinfo>

template<class X, class Y>
int add_i(X x, Y y)
{
    return x + y;
}

template<class X, class Y>
double add_d(X x, Y y)
{
    return x + y;
}

int auto_loop(std::vector<int> v)
{
    // type of i is int&
    for(auto &&i: v)
        std::cout << i << " ";
    std::cout << std::endl;

    return 1;
}

int main() {

    std::vector<int> v = {0, 1, 2, 3, 4, 5};

    int x = 1;
    auto y = &x;
    std::cout << "type of y: " << typeid(y).name() << std::endl;

    auto ret_1 = add_i(1, 2);
    std::cout << "type of ret_1: " << typeid(ret_1).name() << std::endl;
    auto ret_2 = add_d(1, 2.22);
    std::cout << "type of ret_2: " << typeid(ret_2).name() << std::endl;
    auto ret_3 = {1, 2, 3};
    std::cout << "type of ret_3: " << typeid(ret_3).name() << std::endl;
    auto_loop(v);

    return 1;
}
