#include <iostream>
#include <set>

int main() {

    std::set<int> set1;
    std::cout << "set1 is " << (set1.size() ? "not empty" : "empty") << std::endl;
    std::cout << std::endl;

    std::cout << "set2 will initialize the unorder and duplicate int " 
                 "list {2, 2, 5, 4, 1, 3} and show it output as following: " << std::endl;
    std::set<int> set2 = {2, 2, 5, 4, 1, 3};
    for (auto &&i: set2)
        std::cout << i << " ";
    std::cout << std::endl;
    std::cout << std::endl;

    std::cout << "initialize the set3 by copy set2 (using constructor)" << std::endl;
    std::set<int> set3(set2);
    std::cout << "then try to found the item on set3..." << std::endl;
    std::cout << "try to check value `2` on set3... "
              << (set3.count(2) ? "we found it!" : "we could not found it!") << std::endl;
    std::cout << std::endl;

    std::cout << "Now, we will try to find a item on set3 and erase it" << std::endl;
    auto it = set3.find(2);
    set3.erase(it);
    std::cout << "After earse, the set woule be following: " << std::endl;
    for (auto &&i: set3)
        std::cout << i << " ";
    std::cout << std::endl;
    std::cout << std::endl;

    return 1;

}
